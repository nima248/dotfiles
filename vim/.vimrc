" My vimrc
" Lachlan Ahrens

set nocompatible

set mouse=a


"--- LEADER KEY MAPPINGS ---
let mapleader = " "
map <leader>e :Explore<CR>
map <leader>s :Sexplore<CR>
map <leader>v :Vexplore<CR>


"--- INDENTATION ---
set shiftwidth=4    " number of spaces to use for autoindent
set tabstop=4		" number of visual spaces per TAB
set softtabstop=4	" number of spaces in TAB when editing
set expandtab		" pressing TAB inserts spaces
set autoindent      " copy indent from current line when starting new line
set smartindent
filetype indent on
" Python indentation values
let g:pyindent_open_paren = '&sw'
let g:pyindent_nested_paren = '&sw'
let g:pyindent_continue = '&sw * 2'


"--- SPLITS ---
set splitright      " vertical split puts new window on the right
nnoremap gh <C-w>h
nnoremap gj <C-w>j
nnoremap gk <C-w>k
nnoremap gl <C-w>l
"nnoremap <C-h> <C-w>h
"nnoremap <C-j> <C-w>j
"nnoremap <C-k> <C-w>k
"nnoremap <C-l> <C-w>l


"--- APPEARANCE ---
"set t_Co=16                        " use terminal's 16 color pallete
syntax on                          " enable syntax highlighting
so ~/.vim/colors/lightdark.vim     " contains 'set background=<light/dark>'
let g:solarized_termcolors=16      " use terminal's 16 color palette
so ~/.vim/colors/active_theme.vim  " contains 'colorscheme <name.vim>
"=======
" colors
"set t_Co=16              " use terminal's 16 color pallete
syntax enable            " enable syntax highlighting
if has ("win32") || has ("win16")
    colorscheme darkblue
else
    so ~/.vim/colors/lightdark.vim  " contains 'set background=<light/dark>'
    let g:solarized_termcolors=16  " use terminal's 16 color palette
    let g:gruvbox_contrast_dark='hard'
    so ~/.vim/colors/active_theme.vim  " contains 'colorscheme <name.vim>
endif

set laststatus=2         " always show status line
set showcmd              " show in-process commands at bottom of window
set number               " number on left-hand side
set relativenumber
set scrolloff=4     " vertical space from cursor to top/bottom of window


"--- FOLDING ---
set foldmethod=indent
set foldlevelstart=99


"--- FORMATTING ---
"set formatoptions+=a  " automatic reformating of paragraphs
set linebreak       " break lines between words, not in the middle of words
set showbreak=>     " show a '>' at the beginning of a wrapped line
"set colorcolumn=80  " enable colored column
if &background=="dark"
    highlight ColorColumn ctermbg=DarkGray guibg=DarkGray
else
    highlight ColorColumn ctermbg=LightGray guibg=LightGray
end
autocmd FileType make set noexpandtab shiftwidth=4 softtabstop=0
    


"--- AUTOCLOSE PARENS/BRACKETS/BRACES ---
"inoremap (( ()<left>
"inoremap [[ []<left>
"inoremap {{ {}<left>
"inoremap "" ""<left>
"inoremap '' ''<left>
"inoremap << <><left>

inoremap (<CR> (<CR>)<Esc>ko
inoremap [<CR> [<CR>]<Esc>ko
inoremap {<CR> {<CR>}<Esc>ko


"--- SEARCH ---
set incsearch       " highlight pattern as typing
nnoremap <silent> <esc> :nohlsearch<cr>



"--- FILE SEARCH ---
set path+=**
set wildmenu


"--- BACKUP & UNDO ---
set backup
set backupdir=$HOME/.vim/backup//
set undofile
set undodir=$HOME/.vim/undo//


"--- NETRW ---
let g:netrw_banner = 0


"--- FIXES TO WEIRD PROBLEMS ---
" Remove delay when pressing ESC
set timeoutlen=1000 ttimeoutlen=10
" Allow backspace to work properly (see :help 'backspace')
set backspace=indent,eol,start 
" fix starting in replace mode
nnoremap <esc>^[ <esc>^[

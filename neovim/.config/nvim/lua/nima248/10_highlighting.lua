vim.api.nvim_exec([[
    augroup BashAliasesLocal
        autocmd!
        autocmd BufNewFile,BufRead .bash_aliases_local set filetype=sh
    augroup END
]], false)

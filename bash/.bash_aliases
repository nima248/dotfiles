alias c='clear'
alias l='ls -hlF --color'
alias la='ls -AhlF --color'
alias mv='mv -i'
alias rm='safe-rm -I --preserve-root'

alias xclip='xclip -sel clip'
alias diff='diff --color'
alias watch='watch --color '

alias nv='NVIM_APPNAME="kickstart.nvim" nvim'
alias t='tmux'

alias gs='git status'
alias gb='git branch'
alias ga='git add'
alias gc='git commit'
alias gl='git log'
alias gd='git diff'

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

alias h='history'
alias tree='tree --dirsfirst -F'
alias mkdir='mkdir -p -v'

alias im="imv-wayland"

alias d="du -sh * .*"

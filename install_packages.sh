#!/usr/bin/env bash
#
# Basic packages.
# Note: Neovim should be installed using the latest Appimage
#   so that the config remains compatible.

P_BASE=( \
    fuse \
	git \
    mc \
    ncdu \
    safe-rm
	stow \
	tmux \
	tree
)

P_SERVER=( \
    foot-terminfo \
)

P_DESKTOP_COMMON=( \
    network-manager \
    pamixer \
    udiskie \
    fonts-noto-color-emoji \
    exfatprogs \
    exfat-fuse \
    libfido2-dev
)

P_DESKTOP_X=( \
)

P_DESKTOP_WAYLAND=( \
	sway \
	sway-backgrounds \
	swaybg \
	swayidle \
	swaylock \
	xdg-desktop-portal-wlr \
    grim \
	psmisc \
    slurp \
    wl-clipboard \
    wlogout \
    wofi \
    xwayland
)

printf "
    0  Server
    1  Wayland
    2  X

Your choice: "
read CHOICE

if [ $CHOICE = 0 ]; then
    echo Chose server
    PACKAGES=(${P_BASE[@]} ${P_SERVER[@]})
elif [ $CHOICE = 1 ]; then
    echo Chose Wayland
    PACKAGES=(${P_BASE[@]} ${P_DESKTOP_COMMON[@]} ${P_DESKTOP_WAYLAND[@]})
else
    echo Chose X
    PACKAGES=(${P_BASE[@]} ${P_DESKTOP_COMMON[@]} ${P_DESKTOP_X[@]})
fi

echo Packages to install:
echo "${PACKAGES[@]}"
echo Press Enter to continue
read

sudo apt update && sudo apt install "${PACKAGES[@]}"

#!/usr/bin/env bash
#

function print_usage {
	script_name=$(basename "$0")
		printf "Usage:
		$script_name            (stow dotfiles)
		$script_name unstow     (unstow dotfiles)
		$script_name help       (show help)\n"
}


# Bug in stow 2.3.1 results in unnecessary warning
# This wrapper suppresses that output
function stow_suppress_warning {
	stow "$@" \
		2> >(grep -v 'BUG in find_stowed_path? Absolute/relative mismatch' 1>&2)
}


STOW_DIR=$(dirname $0)
	TARGET_DIR=$HOME

	unstow=false
	if [ -n "$1" ]
	then
	if [ $1 = "unstow" ]
	then
	unstow=true
	elif [ $1 = "help" ]
	then
	print_usage
	exit 0
	else
	echo Invalid argument $1
	print_usage
	exit 0
	fi
	fi

	echo Using stow dir:   $STOW_DIR
	echo Using target dir: $TARGET_DIR

	if [ "$unstow" = true ] ;
	then
	echo Unstowing...
	else
	echo Stowing...
	fi

	STOW_LIST=( \
			bash \
			foot \
			kickstart-nvim \
			mc \
			neovim \
			scripts \
			tmux \
		  )


	stow_suppress_warning --verbose=2 -d $STOW_DIR -t $TARGET_DIR -D "${STOW_LIST[@]}"
	if [ "$unstow" = false ] ;
	then
	stow_suppress_warning --verbose=2 -d $STOW_DIR -t $TARGET_DIR "${STOW_LIST[@]}"
	fi

	echo Done

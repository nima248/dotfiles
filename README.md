# Setup new system with dotfiles

Install packages from apt:
	./install_packages.sh

Symlink dotfiles to home directory using stow:
	./stow_dotfiles.sh

# Other configs for new install
## Sway run script

Place the following at /usr/local/bin/sway-run
Make sure to $ chmod +x

    #!/bin/sh

    export XDG_SESSION_TYPE=wayland
    export XDG_SESSION_DESKTOP=sway
    export XDG_CURRENT_DESKTOP=sway

    # Wayland stuff
    export MOZ_ENABLE_WAYLAND=1
    export QT_QPA_PLATFORM=wayland
    export SDL_VIDEODRIVER=wayland
    export _JAVA_AWT_WM_NONREPARENTING=1

    exec sway $@

    #
    # If you use systemd and want sway output to go to the journal, use this
    # instead of the `exec sway $@` above:
    #
    #    exec systemd-cat --identifier=sway sway $@
    #

sway-run.sh will be executed from ~/.bash_profile
